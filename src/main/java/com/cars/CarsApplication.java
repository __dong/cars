package com.cars;

import com.cars.domain.Car;
import com.cars.domain.Customer;
import com.cars.domain.Seller;
import com.cars.repository.*;
import com.cars.sqlScriptRunner.ScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;


@SpringBootApplication
public class CarsApplication{
	private static final Logger log = LoggerFactory.getLogger(CarsApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(CarsApplication.class, args);
	}

	//execute Inserts from .sql
	@Bean
	public CommandLineRunner sqlRunner() {
		return new CommandLineRunner() {

			@Override
			public void run(String... strings) throws Exception {
				// Create MySql Connection
				Class.forName("com.mysql.jdbc.Driver");
				Connection con = DriverManager.getConnection(
						"jdbc:mysql://localhost:3305/db11", "dev", "dev");
				Statement stmt = null;
				try {
					// Initialize object for ScripRunner
					ScriptRunner sr = new ScriptRunner(con, false, false);

					// Give the input file to Reader
					Resource resource = new ClassPathResource("data-brand.sql");
					FileReader reader = new FileReader(resource.getFile());
					log.info("HIERE: " + resource + reader);

					Resource resource2 = new ClassPathResource("data-location.sql");
					FileReader reader2 = new FileReader(resource2.getFile());

					// Exctute script
					// ACTIVATE INSERT HERE // !!!

					//sr.runScript(reader); //insert brands
					//sr.runScript(reader2); //insert locations

				} catch (Exception e) {
					System.err.println("Failed to Execute FileReader/Resource"
							+ " The error is " + e.getMessage());
				}
			}
		};
	}

	@Bean
	public CommandLineRunner demo(CustomerRepository customerRepository,
								  SellerRepository sellerRepository,
								  LocationRepository locationRepository,
								  BrandRepository brandRepository,
								  CarRepository carRepository) {
		return (args) -> {
			//save couple of customers to db
			customerRepository.save(new Customer("Jack", "Bauer"));
			customerRepository.save(new Customer("Chloe", "O'Brian"));
			customerRepository.save(new Customer("Kim", "Bauer"));
			customerRepository.save(new Customer("David", "Palmer"));
			customerRepository.save(new Customer("Michelle", "Dessler"));

			// fetch all customers
			log.info("Customers found with findAll():");
			log.info("-------------------------------");
			for (Customer customer : customerRepository.findAll()) {
				log.info(customer.toString());
			}
			log.info("");

			// fetch an individual customer by ID
			Customer customer = customerRepository.findOne(1L);
			log.info("Customer found with findOne(1L):");
			log.info("--------------------------------");
			log.info(customer.toString());
			log.info("");

			// fetch customers by last name
			log.info("Customer found with findByLastName('Bauer'):");
			log.info("--------------------------------------------");
			for (Customer bauer : customerRepository.findByLastName("Bauer")) {
				log.info(bauer.toString());
			}
			log.info("");

			Seller vader = new Seller();
			vader.setFirstName("Darth");
			vader.setLastName("Vader");
			vader.setLocation(locationRepository.findOne(1));
			sellerRepository.save(vader);

			Car car1 = new Car();
			car1.setLocation(locationRepository.findOne(1));
			car1.setBrand(brandRepository.findOne(2));
			car1.setColor("Metallic black");
			car1.setKm(123);
			car1.setSeller(sellerRepository.findOne(1));
			carRepository.save(car1);
		};
	}
}
