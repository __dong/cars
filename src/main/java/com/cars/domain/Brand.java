package com.cars.domain;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by tdhoang on 05.01.17.
 */
@Entity
public class Brand {


    private int id;
    private String brandName;
    private String model;
    private Integer year;
    private Set<Car> cars;



    public Brand() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @OneToMany(fetch = FetchType.EAGER , mappedBy = "brand", cascade = CascadeType.ALL)
    public Set<Car> getCars() {
        return cars;
    }

    public void setCars(Set<Car> cars) {
        this.cars = cars;
    }

    @Override
    public String toString() {
        return "Brand{" +
                "id=" + id +
                ", brandName='" + brandName + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", cars=" + cars +
                '}';
    }
}
