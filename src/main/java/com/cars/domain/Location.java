package com.cars.domain;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by tdhoang on 05.01.17.
 */
@Entity
@Table (name = "location")
public class Location {


    private int id;

    private String locationName;
    private String postalCode;
    private String state;
    private Set<Car> cars;
    private Set<Seller> sellers;


    public Location() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @OneToMany(fetch = FetchType.EAGER , mappedBy = "location", cascade = CascadeType.ALL)
    public Set<Car> getCars() {
        return cars;
    }

    public void setCars(Set<Car> cars) {
        this.cars = cars;
    }

    @OneToMany(fetch = FetchType.EAGER , mappedBy = "location", cascade = CascadeType.ALL)
    public Set<Seller> getSellers() {
        return sellers;
    }

    public void setSellers(Set<Seller> sellers) {
        this.sellers = sellers;
    }


    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", locationName='" + locationName + '\'' +
                ", postalCode=" + postalCode +
                ", state='" + state + '\'' +
                ", cars=" + cars +
                ", sellers=" + sellers +
                '}';
    }
}
