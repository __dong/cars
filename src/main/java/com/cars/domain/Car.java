package com.cars.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by tdhoang on 05.01.17.
 */

@Entity
@Table (name = "car")
public class Car {

    private int id;
    private String type;
    private Date firstRegistration;
    private int km;
    private Float price;
    private int horsepower;
    private String fuelType;
    private String gear;
    private String color;
    private String badge;
    private Location location;
    private Brand brand;
    private Seller seller;



    public Car() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getFirstRegistration() {
        return firstRegistration;
    }

    public void setFirstRegistration(Date firstRegistration) {
        this.firstRegistration = firstRegistration;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getGear() {
        return gear;
    }

    public void setGear(String gear) {
        this.gear = gear;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBadge() {
        return badge;
    }

    public void setBadge(String badge) {
        this.badge = badge;
    }

    @ManyToOne
    @JoinColumn(name = "location_id")
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @ManyToOne
    @JoinColumn(name = "brand_id")
    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    @ManyToOne
    @JoinColumn(name = "seller_id")
    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }


    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", firstRegistration=" + firstRegistration +
                ", km=" + km +
                ", price=" + price +
                ", horsepower=" + horsepower +
                ", fuelType='" + fuelType + '\'' +
                ", gear='" + gear + '\'' +
                ", color='" + color + '\'' +
                ", badge='" + badge + '\'' +
                ", location=" + location +
                ", brand=" + brand +
                ", seller=" + seller +
                '}';
    }
}
