package com.cars.service;

import com.cars.domain.Seller;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by tdhoang on 05.01.17.
 */

@Transactional
public interface SellerService {
    Seller findByLastName(String lastName);
    void saveSeller(Seller seller);
}
