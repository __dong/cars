package com.cars.repository;

import com.cars.domain.Greeting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by tdhoang on 05.02.17.
 */
@Repository
public interface GreetingRepository extends CrudRepository<Greeting, Long> {
}
