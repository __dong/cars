package com.cars.repository;

import com.cars.domain.Location;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by tdhoang on 05.01.17.
 */
@Repository
public interface LocationRepository extends CrudRepository<Location, Integer> {
}
