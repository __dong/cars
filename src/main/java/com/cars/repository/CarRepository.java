package com.cars.repository;

import com.cars.domain.Car;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by tdhoang on 05.01.17.
 */
@Repository
public interface CarRepository extends CrudRepository<Car, Integer> {
}
