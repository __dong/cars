package com.cars.repository;

import com.cars.domain.Brand;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by tdhoang on 05.01.17.
 */
@Repository
public interface BrandRepository extends CrudRepository<Brand, Integer> {
}
