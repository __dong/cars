package com.cars.repository;

import com.cars.domain.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by tdhoang on 05.02.17.
 */
@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

    List<Customer> findByLastName(String lastName);
}

/*
//REST usage
@RepositoryRestResource(collectionResourceRel = "customer", path = "customer")
interface CustomerRepository2 extends PagingAndSortingRepository<Customer, Long> {
    List<Customer> findByLastName(@Param("name") String name);

}*/