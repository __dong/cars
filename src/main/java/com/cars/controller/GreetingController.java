package com.cars.controller;

import com.cars.domain.Greeting;
import com.cars.repository.GreetingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Created by tdhoang on 05.02.17.
 */

@Controller
public class GreetingController {
/*    private  final String template = "Hello, %s..";
    private AtomicLong counter = new AtomicLong();

    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value="name", defaultValue = "Welt") String name) {
        return  new Greeting(counter.incrementAndGet(),
                    String.format(template, name));
    }
    */
    @Autowired
    GreetingRepository greetingRepository;
    @GetMapping(path = "/greeting")
    public String greetingForm(Model model) {
        model.addAttribute("greeting", new Greeting());
        return "greeting";
    }

    @PostMapping(path = "/greeting")
    public String greetingSubmit(@ModelAttribute Greeting greeting) {
        greetingRepository.save(greeting);
        return "result";
    }
}
