package com.cars.controller;

import com.cars.domain.Customer;
import com.cars.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by tdhoang on 05.02.17.
 */
@Controller
@RequestMapping(path = "/demo")
public class MainController {
    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping(path = "/add")
    public @ResponseBody String addNewCustomer (@RequestParam String firstName, @RequestParam String lastName) {
        Customer customer = new Customer();
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customerRepository.save(customer);
        return "alert(saved)";
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<Customer> getAllCustomers(){
        return customerRepository.findAll();
    }
}
