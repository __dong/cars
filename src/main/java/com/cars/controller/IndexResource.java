package com.cars.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by tdhoang on 05.02.17.
 */
@Controller
public class IndexResource {

    @RequestMapping("/home")
    public String home(){
        return "index";
    }

}
