var app = angular.module('portalWebApp', ["ngRoute"])
app.config(['$routeProvider', function($routeProvider) {
$routeProvider
    .when('/', {templateUrl: './partials/home.html',})
    .when('/verkaufen', {templateUrl: './partials/verkaufen.html',})
    .when('/kaufen', {templateUrl: './partials/kaufen.html',})
    .when('/impressum', {templateUrl: './partials/impressum.html',})
    .when('/datenschutz', {templateUrl: './partials/datenschutz.html',})

    .otherwise({redirectTo: '/'});
}]);